//
//  AppDelegate.swift
//  Decouverte iOS iBeacon
//
//  Created by Michaël Minelli on 22.10.15.
//  Copyright © 2015 Michaël Minelli. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        // Test si la méthode d'iOS 8 est disponible (inutile hors context démo ici car appli pour iOS 8 et +)
        if(UIApplication.instancesRespondToSelector(Selector("registerUserNotificationSettings:")))
        {
            //Enregistre l'application pour la possibilité d'envoyer des notifications local.
            let notificationActionOk :UIMutableUserNotificationAction = UIMutableUserNotificationAction()
            notificationActionOk.identifier = "ACCEPT_IDENTIFIER"
            notificationActionOk.title = "Ok"
            notificationActionOk.destructive = false
            notificationActionOk.authenticationRequired = false
            notificationActionOk.activationMode = UIUserNotificationActivationMode.Background
            
            let notificationCategory:UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
            notificationCategory.identifier = "REGION_CATEGORY"
            notificationCategory.setActions([notificationActionOk], forContext: UIUserNotificationActionContext.Default)
            
            //registerting for the notification.
            application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes:[ .Sound, .Alert, .Badge], categories: nil))
        }
        else
        {
            //Partie pour iOS 7
        }
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

